import { DataSource } from 'typeorm';
import { User } from '../src/users/entities/user.entity';
import { Post } from 'src/posts/entities/post.entity';

export default new DataSource({
  type: 'postgres',
  host: 'postgres',
  port: 5432,
  database: 'app-nest-api',
  username: 'admin',
  password: 'secret',
  migrations: [`${__dirname}/migrations/*{.ts,.js}`],
  entities: [User, Post],
  migrationsTableName: 'migrations',
});
