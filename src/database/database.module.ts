import { Module, Global } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import config from '../config';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [config.KEY],

      useFactory: (configService: ConfigType<typeof config>) => {
        const { user, password, port, db_name, host } = configService.postgres;
        // console.log(`mongodb://${user}:${password}@${host}:${port}/${dbName}`);
        return {
          type: 'postgres',
          host,
          port,
          username: user,
          password,
          database: db_name,
          synchronize: true,
          logging: 'all',
          // entities: [],
          autoLoadEntities: true,
          // migrations: [__dirname + './migrations/*{.ts,.js}'],
        };
      },
    }),
  ],
  exports: [TypeOrmModule],
})
export class DatabaseModule {}
