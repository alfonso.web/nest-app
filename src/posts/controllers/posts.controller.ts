import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { PostsService } from '../services/posts.service';
import { Post as PostEntity } from '../entities/post.entity';
import { CreatePostDto } from '../dtos/post.dto';

@Controller('posts')
export class PostsController {
  constructor(private postsService: PostsService) {}

  @Post(':user_id')
  async create(
    @Body() payload: CreatePostDto,
    @Param('user_id') user_id: number
  ): Promise<PostEntity> {
    return await this.postsService.create(payload, user_id);
  }

  @Get()
  async index(): Promise<PostEntity[]> {
    return await this.postsService.index();
  }
}
