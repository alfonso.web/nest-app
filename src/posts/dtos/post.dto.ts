import { IsNotEmpty, IsOptional, IsPositive, IsString } from 'class-validator';
import { CreateUserDto } from 'src/users/dtos/user.dto';

export class CreatePostDto {
  @IsString()
  @IsNotEmpty()
  readonly title: string;
  @IsString()
  @IsNotEmpty()
  readonly body: string;
  @IsPositive()
  @IsOptional()
  readonly user_id?: CreateUserDto;
}
