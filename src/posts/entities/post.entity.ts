import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity({ name: 'posts' })
export class Post {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('varchar', { length: 100 })
  title: string;
  @Column('varchar', { length: 400 })
  body: string;

  @Column('int', { default: 0 })
  reactions: number;

  @ManyToOne(() => User, (user) => user.posts)
  // @JoinColumn({ name: 'user_id' })
  user: User;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'created_at',
  })
  createdAt: Date;
}
