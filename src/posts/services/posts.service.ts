import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from '../entities/post.entity';
import { Repository } from 'typeorm';
import { CreatePostDto } from '../dtos/post.dto';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post) private postsRepository: Repository<Post>,
    @InjectRepository(User) private usersRepository: Repository<User>
  ) {}
  async create(body: CreatePostDto, user_id: number): Promise<Post> {
    try {
      const user = await this.usersRepository.findOneBy({
        id: user_id,
      });
      if (!user) {
        throw new HttpException(
          `User #${user_id} not found`,
          HttpStatus.BAD_REQUEST
        );
      }

      return await this.postsRepository.save({
        ...body,
        user,
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async index(): Promise<Post[]> {
    const posts: Post[] = await this.postsRepository.find({
      relations: {
        user: true,
      },
    });
    if (posts.length == 0) {
      throw new HttpException(
        'There are no registered posts',
        HttpStatus.BAD_REQUEST
      );
    }
    return posts;
  }
}
