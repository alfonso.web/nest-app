import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  readonly first_name: string;
  @IsString()
  @IsNotEmpty()
  readonly last_name: string;
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;
  @IsString()
  @IsNotEmpty()
  @IsNotEmpty()
  readonly username: string;
  @IsString()
  @IsNotEmpty()
  @Length(10)
  password: string;
}
export class UpdateUserDto {
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;
  @IsString()
  @IsNotEmpty()
  @IsNotEmpty()
  readonly username: string;
  @IsString()
  @IsNotEmpty()
  @Length(10)
  password: string;
}
