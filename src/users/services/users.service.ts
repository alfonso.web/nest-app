import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from '../entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { CreateUserDto, UpdateUserDto } from '../dtos/user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>
  ) {}

  async create(body: CreateUserDto): Promise<User> {
    try {
      body.password = await bcrypt.hash(body.password, +process.env.HASH_SALT);
      const newObject = await this.usersRepository.save(body);

      return newObject;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async index(): Promise<User[]> {
    const users: User[] = await this.usersRepository.find();
    if (users.length == 0) {
      throw new HttpException(
        'There are no registered users',
        HttpStatus.BAD_REQUEST
      );
    }
    return users;
  }

  async update(
    body: UpdateUserDto,
    id: number
  ): Promise<UpdateResult | undefined> {
    try {
      const user = await this.usersRepository.findOneBy({
        id: id,
      });
      if (!user) {
        throw new HttpException(
          `User #${id} not found`,
          HttpStatus.BAD_REQUEST
        );
      }
      const userUpdated: UpdateResult = await this.usersRepository.update(
        id,
        body
      );

      return userUpdated;
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}
